<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index()->comment('用户 id');
            $table->unsignedBigInteger('category_id')->comment('分类 id');
            $table->string('title')->comment('标题');
            $table->text('content');
            $table->timestamp('published_at')->nullable(true)->comment('发布时间');
            $table->unsignedBigInteger('best_answer_id')->nullable();
            $table->unsignedBigInteger('answers_count')->default(0)->comment('回答数量');
            $table->string('slug')->nullable()->comment('标题英文');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
