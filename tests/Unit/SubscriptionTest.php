<?php

namespace Tests\Unit;

use App\Models\Question;
use App\Models\Subscription;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_subscription_belongs_to_a_user()
    {
        $subscription = create(Subscription::class);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $subscription->user());
    }

    /**
     * @test
     */
    public function can_know_it_if_subscribed_to()
    {
        $this->signIn();

        $question = factory(Question::class)->state('published')->create();

        $this->post('/questions/' . $question->id . '/subscriptions');

        $this->assertTrue($question->refresh()->isSubscribedTo(Auth::user()));
    }
}
