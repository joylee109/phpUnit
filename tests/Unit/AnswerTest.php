<?php

namespace Tests\Unit;

use App\Models\Answer;
use App\Models\Comment;
use App\Models\User;
use App\Models\Vote;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AnswerTest extends TestCase
{
    use RefreshDatabase;
    use ActivitiesContractTest;

    public function getActivityModel()
    {
        return create(Answer::class);
    }

    public function getActivityType()
    {
        return 'created_answer';
    }

    /**
     * 测试 Answer 模型中 isBest() 方法
     * @test
     */
    public function it_knows_if_it_is_the_best()
    {
        $answer = create(Answer::class);

        $this->assertFalse($answer->isBest());

        $answer->question->update([
            'best_answer_id' => $answer->id,
        ]);

        $this->assertTrue($answer->isBest());
    }

    /**
     * 测试 Answer 模型中 questions() 对应关系
     * @test
     */
    public function an_answer_belongs_to_a_question()
    {
        $answer = create(Answer::class);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\BelongsTo', $answer->question());
        $this->assertInstanceOf('App\Models\User', $answer->owner);
    }

    /**
     * 点赞方法
     * @test
     */
    public function can_vote_up_an_answer()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->assertDatabaseMissing('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
            'type' => 'vote_up',
        ]);

        $answer->voteUp(Auth::user());

        $this->assertDatabaseHas('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
            'type' => 'vote_up',
        ]);
    }

    /**
     * 取消点赞方法
     * @test
     */
    public function can_cancel_vote_up_an_answer()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $answer->voteUp(auth()->user());

        $answer->cancelVoteUp(auth()->user());

        $this->assertDatabaseMissing('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
        ]);
    }

    /**
     * 可以踩答案
     * @test
     */
    public function can_vote_down_an_answer()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->assertDatabaseMissing('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
            'type' => 'vote_down',
        ]);

        $answer->voteDown(Auth::user());

        $this->assertDatabaseHas('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
            'type' => 'vote_down',
        ]);
    }

    /**
     * 取消踩答案
     * @test
     */
    public function can_cancel_vote_down_answer()
    {
        $this->signIn();

        $answer = create(Answer::class);

        // 踩答案
        $answer->voteDown(Auth::user());

        // 取消踩答案
        $answer->cancelVoteDown(Auth::user());

        $this->assertDatabaseMissing('votes', [
            'user_id' => auth()->id(),
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
        ]);
    }

    /**
     * 测试模型类中的 isVotedDown 方法
     * @test
     */
    public function can_know_it_is_voted_down()
    {
        $user = create(User::class);
        $answer = create(Answer::class);
        create(Vote::class, [
            'user_id' => $user->id,
            'voted_id' => $answer->id,
            'voted_type' => get_class($answer),
            'type' => 'vote_down',
        ]);

        $this->assertTrue($answer->refresh()->isVotedDown($user));
    }

    /**
     * 测试关系：answer comment
     * @test
     */
    public function an_answer_has_many_comments()
    {
        $answer = create(Answer::class);

        create(Comment::class, [
            'commented_id' => $answer->id,
            'commented_type' => $answer->getMorphClass(),
            'content' => 'it is a comment',
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Relations\MorphMany', $answer->comments());
    }

    /**
     * 测试评论答案入库方法
     * @test
     */
    public function can_comment_an_answer()
    {
        $answer = create(Answer::class);

        $answer->comment('it is content', create(User::class));

        $this->assertEquals(1, $answer->refresh()->comments()->count());
    }

    /**
     * 测试获取答案评论的数量
     * @test
     */
    public function can_get_comments_count_attribute()
    {
        $answer = create(Answer::class);

        $answer->comment('it is content', create(User::class));

        $this->assertEquals(1, $answer->refresh()->commentsCount);
    }

    /**
     * @test
     */
    public function can_get_comment_endpoint_attribute()
    {
        $answer = create(Answer::class);

        $answer->comment('it is content', create(User::class));

        $this->assertEquals("/answers/{$answer->id}/comments", $answer->refresh()->commentEndpoint);
    }
}
