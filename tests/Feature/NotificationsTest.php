<?php

namespace Tests\Feature;

use App\Models\Question;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /**
     * 关注话题者，有答案产生时，产生提示.
     * @test
     */
    public function a_notification_is_prepared_when_a_subscribed_question_receives_a_new_answer_by_other_people()
    {
        // 创建一个问题
        $question = create(Question::class, [
            'user_id' => auth()->id()
        ]);

        // 关注该问题
        $question->subscribe(auth()->id());

        // 判断数量
        $this->assertCount(0, auth()->user()->notifications);

        // 自己添加答案
        $question->addAnswer([
            'user_id' => auth()->id(),
            'content' => 'Some reply here.',
        ]);

        // 判断提示数量
        $this->assertCount(0, auth()->user()->fresh()->notifications);

        // 他人添加答案
        $question->addAnswer([
            'user_id' => create(User::class)->id,
            'content' => 'Some reply here2.',
        ]);

        // 判断提示数量
        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    /**
     * @test
     */
    public function a_user_can_fetch_their_unread_notifications()
    {
        $question = create(Question::class, [
            'user_id' => auth()->id()
        ]);

        $question->subscribe(auth()->id());

        $question->addAnswer([
            'user_id' => create(User::class)->id,
            'content' => 'Some reply here.',
        ]);

        $response = $this->get(route('user-notifications.index', ['user' => auth()->user()]));

        $result = $response->data('notifications')->toArray();

        $this->assertCount(1, $result['data']);
        $this->assertEquals(1, $result['total']);
    }

    /**
     * @test
     */
    public function guests_can_not_see_unread_notifications_page()
    {
        Auth::logout();
        $this->withExceptionHandling();
        $this->get(route('user-notifications.index'))
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function clear_all_unread_notifications_after_see_unread_notifications_page()
    {
        $question = create(Question::class, [
            'user_id' => auth()->id(),
        ]);

        $question->subscribe(auth()->id());

        $question->addAnswer([
            'user_id' => create(User::class)->id,
            'content' => 'Some reply here.',
        ]);

        $this->assertCount(1, auth()->user()->fresh()->unreadNotifications);

        $this->get(route('user-notifications.index', ['user' => auth()->user()]));

        $this->assertCount(0, auth()->user()->fresh()->unreadNotifications);
    }
}
