<?php

namespace Tests\Feature\Answers;

use App\Models\Answer;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\VoteUpContractTest;
use Tests\TestCase;

class UpVotesTest extends TestCase
{
    use RefreshDatabase;
    use VoteUpContractTest;

    protected function getAffectModel()
    {
        return Answer::class;
    }

    protected function getVoteUpUri($answer = null)
    {
        return $answer ? "/answers/{$answer->id}/up-votes" : '/answers/1/up-votes';
    }

    protected function upVotes($answer)
    {
        return $answer->refresh()->votes('vote_up')->get();
    }

    protected function getModel()
    {
        return Answer::class;
    }

    /**
     * 游客身份不能点赞
     * @test
     */
    public function guest_can_not_vote_up()
    {
        $this->withExceptionHandling();
        $this->post('/answers/1/up-votes')
            ->assertRedirect('/login');
    }

    /**
     * 登录用户点赞
     * @test
     */
    public function authenticated_user_can_vote_up()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->post("/answers/{$answer->id}/up-votes")
            ->assertStatus(201);

        $this->assertCount(1, $answer->refresh()->votes('vote_up')->get());
    }

    /**
     * 用户取消点赞
     * @test
     */
    public function an_authenticated_user_can_cancel_vote_up()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->post("/answers/{$answer->id}/up-votes");

        $this->assertCount(1, $answer->refresh()->votes('vote_up')->get());

        $this->delete("/answers/{$answer->id}/up-votes");

        $this->assertCount(0, $answer->refresh()->votes('vote_up')->get());
    }

    /**
     * 只能投票一次
     * @test
     */
    public function can_vote_up_only_once()
    {
        $this->signIn();

        $answer = create(Answer::class);

        try {
            $this->post('/answers/' . $answer->id . '/up-votes');
            $this->post('/answers/' . $answer->id . '/up-votes');
        } catch(\Exception $e) {
            $this->fail('Can not vote up twice.');
        }

        $this->assertCount(1, $answer->refresh()->votes('vote_up')->get());
    }

    /**
     * 用户点赞
     * @test
     */
    public function can_konw_it_is_voted_up()
    {
        $this->signIn();
        $answer = create(Answer::class);
        $this->post("/answers/{$answer->id}/up-votes");
        $this->assertEquals(1, $answer->refresh()->upVotesCount);

        $this->signIn(create(User::class));
        $this->post("/answers/{$answer->id}/up-votes");

        $this->assertEquals(2, $answer->refresh()->upVotesCount);
    }
}
