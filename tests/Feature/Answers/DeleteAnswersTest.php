<?php

namespace Tests\Feature\Answers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteAnswersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 游客身份不能删除答案
     * @test
     */
    public function guests_cannot_delete_answers()
    {
        $answer = create(Answer::class);

        $this->withExceptionHandling()
            ->delete(route('answers.destroy', ['answer' => $answer]))
            ->assertRedirect('login');
    }

    /**
     * 非法的认证用户不能删除答案
     * @test
     */
    public function unauthorized_users_cannot_delete_answers()
    {
        $this->signIn()->withExceptionHandling();

        $answer = create(Answer::class);

        $this->delete(route('answers.destroy', ['answer' => $answer]))
            ->assertStatus(403);
    }

    /**
     * 已认证用户可以删除答案
     * @test
     */
    public function authorized_users_can_delete_answers()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $answer = create(Answer::class, ['user_id' => auth()->id()]);

        $this->delete(route('answers.destroy', ['answer' => $answer]))->assertStatus(302);

        $this->assertDatabaseMissing('answers', ['id' => $answer->id]);
    }
}
