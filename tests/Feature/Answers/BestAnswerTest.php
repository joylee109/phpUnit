<?php

namespace Tests\Feature\Answers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BestAnswerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 游客身份不能标记最佳答案
     * @test
     */
    public function guests_can_not_mark_best_answer()
    {
        $this->withExceptionHandling();
        // 假设问题存在
        $question = create(Question::class);

        // 创建两个答案
        $answers = create(Answer::class, ['question_id' => $question->id], 3);

        // 发起请求，将其中一个设为最佳答案
        $this->post(route('best-answers.store', ['answer' => $answers[1]]), [$answers[1]])
            ->assertRedirect('/login');
    }

    /**
     * 认证用户标记最佳答案
     * @test
     */
    public function can_mark_one_answer_as_the_best()
    {
        // 模拟登录
        $this->signIn();
        // 创建一个问题
        $question = create(Question::class, ['user_id' => auth()->id()]);
        // 创建答案
        $answers = create(Answer::class, ['question_id' => $question->id], 3);

        $this->assertFalse($answers[0]->isBest());
        $this->assertFalse($answers[1]->isBest());

        $this->postJson(route('best-answers.store', ['answer' => $answers[1]]), [$answers[1]]);

        $this->assertFalse($answers[0]->fresh()->isBest());
        $this->assertTrue($answers[1]->fresh()->isBest());
    }

    /**
     * 问题创建者才能标记最佳答案
     * @test
     */
    public function only_the_question_creator_can_mark_a_best_answer()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $question = create(Question::class, ['user_id' => auth()->id()]);

        $answer = create(Answer::class, ['question_id' => $question->id]);

        // 另一个用户
        $this->signIn(create(User::class));

        $this->postJson(route('best-answers.store', ['answer' => $answer]), [$answer])
            ->assertStatus(403);

        $this->assertFalse($answer->fresh()->isBest());
    }
}
