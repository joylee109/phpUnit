<?php

namespace Tests\Feature\Answers;

use App\Models\Answer;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\VoteDownContractTest;
use Tests\TestCase;

class DownVotesTest extends TestCase
{
    use RefreshDatabase;
    use VoteDownContractTest;

    protected function getVoteDownUri($answer = null)
    {
        return $answer ? "/answers/{$answer->id}/down-votes" : '/answers/1/down-votes';
    }

    protected function getModel()
    {
        return Answer::class;
    }

    protected function downVotes($answer)
    {
        return $answer->refresh()->votes('vote_down')->get();
    }


    /**
     * 踩答案
     * @test
     */
    public function guest_can_not_vote_down()
    {
        $this->withExceptionHandling();
        $this->post('/answers/1/down-votes')
            ->assertRedirect('/login');
    }

    /**
     * 已登录用户踩答案
     * @test
     */
    public function authenticated_user_can_vote_down()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->post("/answers/{$answer->id}/down-votes")
            ->assertStatus(201);

        $this->assertCount(1, $answer->refresh()->votes('vote_down')->get());
    }

    /**
     * 已认证用户可以取消 踩
     * @test
     */
    public function an_authenticated_user_can_cancel_vote_down()
    {
        $this->signIn();

        $answer = create(Answer::class);

        // 踩答案
        $this->post("/answers/{$answer->id}/down-votes");

        $this->assertCount(1, $answer->refresh()->votes('vote_down')->get());

        // 删除踩答案
        $this->delete("/answers/{$answer->id}/down-votes");

        $this->assertCount(0, $answer->refresh()->votes('vote_down')->get());
    }

    /**
     * 一个用户只能踩一次
     * @test
     */
    public function can_vote_down_only_once()
    {
        $this->signIn();

        $answer = create(Answer::class);

        try {
            $this->post("/answers/{$answer->id}/down-votes");
            $this->post("/answers/{$answer->id}/down-votes");
        } catch (\Exception $e) {
            $this->fail('Can not vote down twice.');
        }

        $this->assertCount(1, $answer->refresh()->votes('vote_down')->get());
    }

    /**
     * 查看用户是否已经 踩了答案
     * @test
     */
    public function can_know_it_is_voted_down()
    {
        $this->signIn();

        $answer = create(Answer::class);

        $this->post("/answers/{$answer->id}/down-votes");

        $this->assertTrue($answer->refresh()->isVotedDown(Auth::user()));
    }

    /**
     * 获取反对数量
     * @test
     */
    public function can_know_votes_count()
    {
        $answer = create(Answer::class);

        $this->signIn();
        $this->post("/answers/{$answer->id}/down-votes");
        $this->assertEquals(1, $answer->refresh()->downVotesCount);

        $this->signIn(create(User::class));
        $this->post("/answers/{$answer->id}/down-votes");

        $this->assertEquals(2, $answer->refresh()->downVotesCount);
    }
}
