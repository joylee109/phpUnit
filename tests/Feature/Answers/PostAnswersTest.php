<?php

namespace Tests\Feature\Answers;

use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostAnswersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function user_can_post_an_answer_to_a_published_question()
    {
        // 假设已经存在某个问题
        $question = factory(Question::class)->state('published')->create();
        // $this->actingAs($user = factory(User::class)->create());
        // $this->actingAs($user = create(User::class));
        $this->signIn($user = create(User::class));

        // 然后我们触发某个路由进行评论
        $response = $this->post('/questions/' . $question->id . '/answers' , [
            'user_id' => $user->id,
            'content' => 'This is an answer.',
        ]);

        // 我们要看到预期结果
        $response->assertStatus(302);
        $answer = $question->answers()->where('user_id', $user->id)->first();
        $this->assertNotNull($answer);

        $this->assertEquals(1, $question->answers()->count());
    }

    /**
     * @test
     */
    public function user_can_not_post_an_answer_to_an_unpublished_question()
    {
        // 假设已经存在某个问题
        $question = factory(Question::class)->state('unpublished')->create();

        // $this->actingAs($user = factory(User::class)->create());
        // $this->actingAs($user = create(User::class));
        $this->signIn($user = create(User::class));

        // 对这个问题进行评论
        $response = $this->withExceptionHandling()
            ->post('/questions/' . $question->id . '/answers', [
                'user_id' => $user->id,
                'content' => 'This is an answer.',
            ]);

        // 断言，做出判断
        $response->assertStatus(404);

        $this->assertDatabaseMissing('answers', ['question_id' => $question->id]);
        $this->assertEquals(0, $question->answers()->count());
    }

    /**
     * @test
     */
    public function content_is_required_to_post_answers()
    {
        $this->withExceptionHandling();
        // 假设已经存在某个问题 和 某个人员
        $question = factory(Question::class)->state('published')->create();
        // $this->actingAs($user = factory(User::class)->create());
        // $this->actingAs($user = create(User::class));
        $this->signIn($user = create(User::class));

        // 发起评论
        $response = $this->post('/questions/' . $question->id . '/answers', [
            'user_id' => $user->id,
            'content' => null,
        ]);

        $response->assertRedirect();
        $response->assertSessionHasErrors('content');
    }

    /**
     * @test
     */
    public function signed_in_user_can_post_an_answer_to_a_published_question()
    {
        // 假设已经存在 某个问题 和 某个人员
        $question = factory(Question::class)->state('published')->create();
        // $this->actingAs($user = factory(User::class)->create());
        $this->signIn($user = create(User::class));
        // $user = factory(User::class)->create();

        $response = $this->post('/questions/' . $question->id . '/answers', [
            'content' => 'this is a answer.',
        ]);

        $response->assertStatus(302);

        $answer = $question->answers()->where('user_id', $user->id)->first();
        $this->assertNotNull($answer);

        $this->assertEquals(1, $question->answers()->count());
    }

    /**
     * @test
     */
    public function guests_may_not_post_an_answer()
    {
        $this->withExceptionHandling();
        // 假设生成一个问题
        $question = factory(Question::class)->state('published')->create();

        $response = $this->post('/questions/' . $question->id . '/answers', [
            'content' => 'This is a answer.',
        ]);

        $response->assertStatus(302)
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function guests_may_not_post_an_answer_other_method()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        // 假设生成一个问题
        $question = factory(Question::class)->state('published')->create();

        $response = $this->post('/questions/' . $question->id . '/answers', [
            'content' => 'This is a answer.',
        ]);

        $response->assertStatus(302)
            ->assertRedirect('/login');
    }
}
