<?php

namespace Tests\Feature\Questions;

use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscribeQuestionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 游客身份不能关注和取消关注问题
     * @test
     */
    public function guests_may_not_subscribe_to_or_unsubscribe_from_questions()
    {
        $this->withExceptionHandling();

        $question = create(Question::class);

        $this->post('/questions/' . $question->id . '/subscriptions')
            ->assertRedirect('/login');

        $this->delete('/questions/' . $question->id . '/subscriptions')
            ->assertRedirect('/login');
    }

    /**
     * 登录用户关注话题
     * @test
     */
    public function a_user_can_subscribe_to_questions()
    {
        $this->signIn();

        $question = factory(Question::class)->state('published')->create();

        $this->post('/questions/' . $question->id . '/subscriptions');

        $this->assertCount(1, $question->subscriptions);
    }

    /**
     * 登录用户可以取消关注话题
     * @test
     */
    public function a_user_can_unsubscribe_from_questions()
    {
        $this->signIn();

        $question = factory(Question::class)->state('published')->create();

        $this->post('/questions/' . $question->id . '/subscriptions' );
        $this->delete('/questions/' . $question->id . '/subscriptions');

        $this->assertCount(0, $question->subscriptions);
    }

    /**
     * @test
     */
    public function can_know_subscriptions_count()
    {
        $question = factory(Question::class)->state('published')->create();

        $this->signIn();

        $this->post('/questions/' . $question->id . '/subscriptions');
        $this->assertEquals(1, $question->refresh()->subscriptionsCount);

        // 切换成另外一个用户登录
        $this->signIn(create(User::class));
        $this->post('/questions/' . $question->id . '/subscriptions');

        $this->assertEquals(2, $question->refresh()->subscriptionsCount);
    }
}
