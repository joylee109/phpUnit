<?php

namespace Tests\Feature\Questions;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ViewQuestionsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_questions()
    {
        // 0. 抛出异常
        $this->withoutExceptionHandling();
        // 1. 假设 /questions 路由存在
        // 2. 访问链接 /questions
        $test = $this->get('/questions');

        // 3. 正常返回 200
        $test->assertStatus(200);
    }

    /** @test */
    public function user_can_view_a_single_question()
    {
        // 1. 创建一个问题
        $category = create(Category::class);
        $question = factory(Question::class)->create(['published_at' => Carbon::now(), 'category_id' => $category->id]);

        // 2. 访问链接
        $test = $this->get('/questions/' . $category->slug . '/' . $question->id);

        // 3. 那么应该看到问题的内容
        $test->assertStatus(200)
            ->assertSee($question->title)
            ->assertSee($question->content);
    }

    /**
     * @test
     */
    public function user_can_view_a_published_question()
    {
        $category = create(Category::class);
        // 创建一个问题
        $question = factory(Question::class)->create([
            'published_at' => Carbon::parse('-1 week'),
            'category_id' => $category->id,
        ]);

        // 访问链接
        $test =  $this->get('/questions/' . $category->slug . '/' . $question->id);

        // 对获取到的响应进行判断
        $test->assertStatus(200)
            ->assertSee($question->title)
            ->assertSee($question->conent);
    }

    /**
     * @test
     */
    public function user_cannot_view_unpublished_question()
    {
        $category = create(Category::class);
        // 创建一个问题
        $question = factory(Question::class)->create([
            'published_at' => null,
            'category_id' => $category->id,
        ]);

        // 访问问题的链接
        $test = $this->withExceptionHandling()->get('/questions/' . $category->slug . '/' . $question->id);

        // 断言返回状态码
        $test->assertStatus(404);
    }

    /**
     * 查看问题时，可以查看所有答案
     * @test
     */
    public function can_see_answers_when_view_a_published_question()
    {
        $category = create(Category::class);
        $question = factory(Question::class)->state('published')->create([
            'category_id' => $category->id,
        ]);
        create(Answer::class, ['question_id' => $question->id], 40);

        $response = $this->get('/questions/' . $category->slug . '/' . $question->id);

        $result = $response->data('answers')->toArray();

        $this->assertCount(20, $result['data']);
        $this->assertEquals(40, $result['total']);

        /*
        $answerShouldSee = Answer::find(1);
        $answerShouldNotSee = Answer::find(11);

        $response->assertStatus(200)
            ->assertSee($answerShouldSee->content)
            ->assertDontSee($answerShouldNotSee->content);
        */
    }
}
