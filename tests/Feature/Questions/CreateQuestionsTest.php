<?php

namespace Tests\Feature\Questions;

use App\Models\Category;
use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateQuestionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 游客身份不能创建问题
     * @test
     */
    public function guests_may_not_create_questions()
    {
        $this->withExceptionHandling();
        $this->post('/questions', [])
            ->assertRedirect('/login');
    }

    /**
     * 已登录用户可以创建问题
     * @test
     */
    public function an_authenticated_user_can_create_new_questions()
    {
        $this->signIn();

        $question = make(Question::class);

        $this->assertCount(0, Question::all());

        $this->post('/questions', $question->toArray());

        $this->assertCount(1, Question::all());
    }

    /**
     * 新建问题时，标题必填
     * @test
     */
    public function title_is_required()
    {
        $this->signIn()->withExceptionHandling();

        $response = $this->post('/questions', ['title' => null]);

        $response->assertRedirect();
        $response->assertSessionHasErrors('title');
    }

    /**
     * 新建问题时，内容必填
     * @test
     */
    public function content_is_required()
    {
        $this->signIn()->withExceptionHandling();

        $response = $this->post('/questions', ['content' => null]);

        $response->assertRedirect();
        $response->assertSessionHasErrors('content');
    }

    /**
     * 新建问题时，分类 id 必填
     * @test
     */
    public function category_id_is_required()
    {
        $this->signIn()->withExceptionHandling();

        $question = make(Question::class, ['category_id' => null]);

        $response = $this->post('questions', $question->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors('category_id');
    }

    /**
     * 新建问题时，分类 id 必须有效
     * @test
     */
    public function category_id_is_existed()
    {
        $this->signIn()->withExceptionHandling();

        create(Category::class, ['id' => 1]);

        $question = make(Question::class, ['category_id' => 999]);

        $response = $this->post('/questions', $question->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors('category_id');
    }

    /**
     * 登录用户必须先验证邮箱才能创建问题
     * @test
     */
    public function authenticated_users_must_confirm_email_address_before_creating_questions()
    {
        $this->signIn(create(User::class, ['email_verified_at' => null]));

        $question = make(Question::class);

        $this->post('/questions', $question->toArray())
            ->assertRedirect(route('verification.notice'));
    }

    /**
     * 创建问题的时候，创建 slug
     * @test
     */
    public function get_slug_when_create_a_question()
    {
        $this->signIn();

        $question = make(Question::class, [
            'title' => '英语 英语'
        ]);

        $this->post('/questions', $question->toArray());

        $storedQuestion = Question::first();

        $this->assertEquals('english-english', $storedQuestion->slug);
    }
}
