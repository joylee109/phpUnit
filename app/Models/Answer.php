<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use \App\Models\Traits\VoteTrait;
    use \App\Models\Traits\CommentTrait;
    use \App\Models\Traits\RecordActivityTrait;

    protected $table = 'answers';

    protected $guarded = ['id'];

    protected $appends = [
        'upVotesCount',
        'downVotesCount',
        'commentsCount',
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($reply) {
            $reply->question->increment('answers_count');
        });
    }

    /**
     * 答案 - 问题 对应关系
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * 判断答案是否为最佳答案
     * @return bool
     */
    public function isBest()
    {
        return $this->id == $this->question->best_answer_id;
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
