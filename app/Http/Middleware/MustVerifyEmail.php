<?php

namespace App\Http\Middleware;

use App\Http\Middleware\MustVerifyEmail;
use App\Models\User;
use Auth;
use Closure;

class MustVerifyEmail
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        if (Auth::user()->email_verified_at == null) {
            return redirect('/email/verify');
        }

        return $next($request);
    }
}
